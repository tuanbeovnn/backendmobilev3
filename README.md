<h1>ONLINE ORDERING MOBILE</h1>
<h3>Tuan Nguyen - e1801917</h3>


<h1>Analysis</h1>

<h2>Case description</h2> 


<ul>

<li>Actors and their roles</li>

| Actors | Roles |
| ------ | ------ |
| Client | The customer buy the mobile phone by ordering the phone through click add to cart.|
| System manager | The person can manage the system of website and edit some function as well as maintaining. | 
| Producer | The supplier from many sources such as : Apple, Samsung, Oppo, Huawei…. | 
| Staff  | The staff can be assign to mange each ordering | 
| Role  | The role of staff can be assign to mange each ordering | 


<li>Tasks the actors would like to do</li>


| Tasks | Description |
| ------ | ------ |
| Manager products| This place can add product, and remove product, edit product and search product by brand|
| Product category | To show the list of products and add the products to the cart. The customer can see the details of products by brand and price| 
| Rating | The customer can rate their products by click and choosing rating. | 
| Cart | The cart can show the list of products which are ordering by customers | 
| Payment |  The payment has different method by picking up or paying online. | 
 
<li>Data input</li>

* items object Json
```
{
	"name": "String",
    "price": 0,
    "description": "String",
    "rating": 2,
    "image": "String"
	"producer_code": string
}
```

* Producer objects Json

```
{
	"id": "0,
	"code": "string",
	"producer": "string",
}
```
<li>Data output</li>

* HTTP response status.

<h3>What kind of algorithms there are</h3>
In this project, I will use some basic algorithms to handle requests:

* Searching: use for search products

<h2>Table of functional requirements</h2>

| ID | Description | Priority (1 = must, 2 = should, 3= nice to have) |
| ------ | ------ |------ |
| 1 | Show a list of products  | 1 | 
| 2 | Show a table of manage ordering | 1 |
| 3 | Edit booking | 1 | 
| 4 | Edit items | 1 |
| 5 | Search items | 1 |
| 6 | Search bookings | 1 |
| 7 | Add items | 1 |
| 8 | Add bookings | 1 |
| 9 | Delete items | 1 |
| 10 | Delete bookings | 1 |
| 11 | Picking up orders | 2 |
| 12 | Manage Staff | 2 |
| 13 | Manage Products List | 2 |
| 14 | Mange Client Account | 2 |
| 15 | Seo Products | 2 |
<h2>Project schedule</h2>v
<h2>Project plan document (including the schedule) using the template</h2>
![my image](pics/project.PNG "Package")

<h3>General Description of the Project</h3>

*  This project is final project on “Software Engineering method” course. This project contains the model for the Online Booking Product system (food, clothes,..) , that allows suppliers to sell their products directly online to customers.

<h3>The Aim of the Project</h3>

*  The final product should be a web application that has user-friendly interface where users can easily navigate and use all the functions, including booking and viewing the products, as well as giving more specific descriptions and searching for a certain product. Additionally, the status of product (available or not,..) should be shown in real time.

<h3>Project team </h3>

| Name | Role | Email |
| ------ | ------ |------ |
| Tuan Nguyen Nguyen | Developer | e1801917@edu.vamk.fi | 

<h3>Resouce and time allocation </h3>

<h3>The tools and devices needed for the project are:</h3>

* Visual Paradigm 14.0
* Sublimetext
* MS Project 2016
* GitHub Git
* ECLIPSE

<h3>Documentation Methods and Tools:</h3>

* Programming Languages: Java, Javascript.
* Libraries: Spring, React
* Control system: Git
* Documentation: Java Docs, Spring.io, Reactjs.org.,...

<h3>Working Methods</h3>

*  The project proceeds according to the Scrum framework. 

  
<h3>Work Roles and Areas of Responsibility</h3>

| Name | Role | Responsibility |
| ------ | ------ |------ |
| Tuan Nguyen | Manager | Full-stack developer, documentations | 

<h3>Reference List</h3>

*  Materials from “Software Engineering method” course.


<h3>Project Control Plan</h3> 

*  The tasks are marked as “high priority” should be completed first. Every single step need to be tested before releasing, the test plan should be written in clear ways. 





<h3>Use case diagram drawn with Visual Paradigm</h2>
![my image](pics/usecase.png "Package")

<h1>Design</h1>


<h2>Package diagram drawn with Visual Paradigm</h2>
![my image](pics/package.png "Package")

<h2>Class diagrams (one from client side one from server side)  drawn with Visual Paradigm</h2>
![my image](pics/class.PNG "Package")


<h1>Implementation</h1>

<h2>Java Spring Boot back end having minimum two tables</h2>
![my image](pics/database.png "Package")
<h2>Reactjs front end having minimum two tables fetching the database data</h2>


<h1>Documentation</h1>
<ul>
<li>Swagger documentation for RESTful API</li>
<li>README.md documentation for Spring Boot backend</li>
<li>README.md documentation for React front end (different gitlab -project)</li>
</ul>
<h1>Deployment</h1>
<ul>
<li>Back end deployed to Heroku : https://mobilevamk.herokuapp.com/products</li>
<li>Front end deployed to Heroku : https://mobilevamkvv.herokuapp.com/</li>
</ul>

package mobileweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mobileweb.entity.ProductEntity;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

	void deleteById(long id);

	ProductEntity findById(long id);
	
}

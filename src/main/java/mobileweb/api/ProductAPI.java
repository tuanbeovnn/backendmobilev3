package mobileweb.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mobileweb.dto.ProductDTO;
import mobileweb.entity.ProductEntity;
import mobileweb.service.IProductService;
import mobileweb.service.impl.ProductService;

@CrossOrigin
@RestController
public class ProductAPI {
	
	@Autowired
	private IProductService productService;
	
	@Autowired
	private ProductService service;
	
	@PostMapping(value="/products")
	public ProductDTO createProduct(@RequestBody ProductDTO model) {
		return productService.save(model);
	}
	
	@PutMapping(value="/products/{id}")
	public ProductDTO updateProduct(@RequestBody ProductDTO model, @PathVariable("id") long id) {
		model.setId(id);
		return productService.save(model);
	}
	@DeleteMapping(value="/products/{id}")
	public void deleteProduct(@PathVariable("id") long id) {
		service.delete(id);
		
	}
	@GetMapping(value = "/products")
	public List<ProductDTO> showListProduct() {
		 
		return productService.findAll() ;
	}
	@GetMapping(value = "/products/{id}")
	public ResponseEntity<ProductEntity> getProductById(@PathVariable("id") Long id) {
		ProductEntity productEntity = productService.getProductById(id);
		return new ResponseEntity<ProductEntity>(productEntity,HttpStatus.OK ) ;
	}
}
